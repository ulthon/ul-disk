define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'storage.chunks_position/index',
        add_url: 'storage.chunks_position/add',
        edit_url: 'storage.chunks_position/edit',
        delete_url: 'storage.chunks_position/delete',
        export_url: 'storage.chunks_position/export',
        modify_url: 'storage.chunks_position/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'chunk_md5', title: 'chunk_md5'},                    {field: 'storage_position_id', title: 'storage_position_id'},                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});