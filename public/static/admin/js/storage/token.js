define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'storage.token/index',
        add_url: 'storage.token/add',
        edit_url: 'storage.token/edit',
        delete_url: 'storage.token/delete',
        export_url: 'storage.token/export',
        modify_url: 'storage.token/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},                    {field: 'token', title: 'token'},                    {field: 'expire_time', title: '过期时间'},                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});