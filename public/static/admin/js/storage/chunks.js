define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'storage.chunks/index',
        add_url: 'storage.chunks/add',
        edit_url: 'storage.chunks/edit',
        delete_url: 'storage.chunks/delete',
        export_url: 'storage.chunks/export',
        modify_url: 'storage.chunks/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'storage_path_id', title: 'storage_path_id'},                    {field: 'sort', title: 'sort', edit: 'text'},                    {field: 'chunk_md5', title: 'chunk_md5'},                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});