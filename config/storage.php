<?php

use think\facade\App;

return [
    'host_key'=>get_host_key(),
    'chunk_cache_path' => App::getRootPath() . '/data/storage/uldisk/temp/chunk/',
    'file_cache_path' => App::getRootPath() . '/data/storage/uldisk/temp/file/',
];
