<?php

namespace app\admin\model;

use app\common\model\TimeModel;

class HomeUser extends TimeModel
{

    protected $name = "home_user";

    protected $deleteTime = "delete_time";

    public static $autoCache = [
        [
            'name' => "read_by_username",
            'field' => 'username'
        ]
    ];
}
