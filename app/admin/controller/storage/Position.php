<?php

namespace app\admin\controller\storage;

use app\common\controller\AdminController;
use app\common\tools\FileHandlerTools;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use SebastianBergmann\Timer\Timer;
use think\App;
use think\helper\Str;

/**
 * @ControllerAnnotation(title="storage_position")
 */
class Position extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\StoragePosition();

        $this->assign('select_list_type', $this->model::SELECT_LIST_TYPE, true);

        $this->assign('select_list_status', $this->model::SELECT_LIST_STATUS, true);
    }


    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            $count = $this->model
                ->withJoin(['storagePositionGroup'], 'left')
                ->where($where)
                ->count();
            $list = $this->model
                ->withJoin(['storagePositionGroup'], 'left')
                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    public function testStorage($id)
    {
        $model_position = $this->model->find($id);


        if ($this->request->isAjax()) {


            $type = $this->request->get('type');

            $size = $this->request->get('size', 10);
            $content = '';

            if ($type == 'write') {
                $content = str_pad('', 1024 * 1024 * $size, 'uldisk');
            }

            $total_timer = new Timer();
            $total_timer->start();

            $file_handler_tools = FileHandlerTools::buildInstance($model_position);

            $flysystem = $file_handler_tools->getFlysystem();

            $test_file_path = 'uldisk-test-file/content.template';

            $sotrage_path = $file_handler_tools->buildPath($test_file_path);


            $timer = new Timer();
            $timer->start();

            $result_msg = '';

            try {

                if ($type == 'write') {
                    $flysystem->write($sotrage_path, $content);
                } else if ($type == 'read') {
                    $content = $flysystem->read($sotrage_path);
                } else if ($type == 'delete') {
                    $flysystem->delete($sotrage_path);
                }
                $result_msg = '成功';
            } catch (\Throwable $th) {
                //throw $th;
                $result_msg = '失败：' . $th->getMessage();
            }


            $duration = $timer->stop();
            $total_duration = $total_timer->stop();

            $time = $duration->asString();


            $content_size = strlen($content);
            $rate = $content_size / $duration->asSeconds();

            return json_message([
                'msg' => $result_msg,
                'time' => $time,
                'path' => $sotrage_path,
                'size' =>  convert_size_readable($content_size),
                'rate' => convert_size_readable($rate) . '/s',
                'total_time' => $total_duration->asString(),

            ]);
        }

        // $this->error('存储出错');

        // $this->success('读取出错');

        return $this->fetch();
    }
}
