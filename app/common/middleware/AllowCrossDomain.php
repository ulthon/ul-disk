<?php

namespace app\common\middleware;

use think\middleware\AllowCrossDomain as MiddlewareAllowCrossDomain;

class AllowCrossDomain extends MiddlewareAllowCrossDomain
{
    protected $header = [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Credentials' => 'true',
        'Access-Control-Max-Age'           => 1800,
        'Access-Control-Allow-Methods'     => 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers'     => 'Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-CSRF-TOKEN, X-Requested-With, SESSION-TOKEN',
    ];

    public function getHeader()
    {
        return $this->header;
    }
}
