<?php

namespace app\common\webdav\collection;

use Sabre\DAV\Collection;
use Sabre\DAV\Auth\Plugin as AuthPlugin;
use app\admin\model\StorageLocks;
use app\admin\model\HomeUser;
use app\admin\model\StoragePath;
use app\common\service\StorageService;
use app\common\tools\PathTools;
use app\common\webdav\auth\backend\Model;
use app\common\webdav\fs\Directory;
use app\common\webdav\fs\File;
use app\common\webdav\properties\Handler;
use app\common\webdav\service\Server;
use app\Request;
use League\Flysystem\Adapter\Local;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;
use think\Response;
use Sabre\DAV;
use Sabre\DAV\Auth\Backend\BasicCallBack;
use Sabre\DAV\SimpleCollection;
use think\facade\App;
use think\facade\Log;
use Sabre\DAV\Auth;
use Sabre\DAV\Exception\Forbidden;
use Sabre\DAV\Locks\Backend\PDO;
use think\facade\Config;
use think\facade\Db;


class Home extends Directory
{

    protected $modelUser = null;

    function __construct(HomeUser $model_user)
    {
        $this->modelUser = $model_user;

        $model_path = StoragePath::where('path', $model_user->home_path)->find();

        if (empty($model_path)) {
            $model_path = new StoragePath();
            $model_path->path = $model_user->home_path;
            $model_path->type = 'collection';
            $model_path->user_id = $this->modelUser->id;
            $model_path->save();
        }

        $this->currentModelPath = $model_path;

        $this->propertiesHandler = new Handler($model_path);

        $this->storageService = new StorageService($model_user, false);
    }


    public function delete()
    {
        throw new Forbidden('This node cannot be delete');
    }

    public function getName()
    {
        return 'dav';
    }

    public function setName($name)
    {
        throw new Forbidden('This node cannot be renamed');
    }

    public function getLastModified()
    {
        return null;
    }
}
