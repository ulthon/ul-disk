<?php

namespace app\common\webdav\collection;

class Video extends Home
{
    public function getName()
    {
        return 'video';
    }

    public function getChildren()
    {
        return [];
    }
}
