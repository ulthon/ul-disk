<?php

namespace app\common\webdav\exception;

class NotAuthenticated extends ResponseException
{
    protected $httpCode = 401;
    protected $httpHeaders = [];
}
