<?php

namespace app\common\webdav\properties;

use app\admin\model\StoragePath;
use think\facade\Log;

class DavHandler
{

    protected $modelPath;

    public function __construct($model_path)
    {
        $this->modelPath = $model_path;
    }

    public function creationDate()
    {
        return date('c', $this->modelPath->getOrigin('create_time'));
    }
    public function displayName()
    {
        return $this->modelPath->path_name;
    }

    public function getContentLength()
    {
        return $this->modelPath->size;
    }
    public function getContentType()
    {
        return $this->modelPath->content_type;
    }
    public function getEtag()
    {
        return $this->modelPath->chunk_list_md5;
    }

    // TODO:这个方法似乎只需要返回当前目录或节点占用的空间，或者节点占用的空间，不需要遍历子元素
    public function quotaUsedBytes()
    {
        Log::debug('call quota used');
        return $this->modelPath->quota_used_bytes;
    }
}
