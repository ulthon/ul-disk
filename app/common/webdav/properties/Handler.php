<?php

namespace app\common\webdav\properties;

use think\facade\Log;
use think\helper\Str;

class Handler
{

    protected $modelPath;

    protected $handlerDav;

    public function __construct($model_path)
    {
        $this->modelPath = $model_path;

        $this->handlerDav = new DavHandler($model_path);
    }

    public function setProperty($name, $value)
    {
    }


    public function getProperty($name)
    {
        // Log::debug($name);
        $value = null;
        if (strpos($name, '{DAV:}')  === 0) {
            $method = substr($name, 6);

            $method = str_replace('-','_',$method);

            $method = Str::camel($method);

            // Log::debug($method);
            if (method_exists($this->handlerDav, $method)) {
                $value = $this->handlerDav->$method();
                // Log::debug('method value:' . $value);
            };
        }


        return $value;
    }
}
