<?php
// 应用公共文件

use app\common\service\AuthService;
use app\common\tools\PathTools;
use think\facade\App;
use think\facade\Cache;
use think\route\Url;

if (!function_exists('__url')) {

    /**
     * 构建URL地址
     * @param string $url
     * @param array $vars
     * @param bool $suffix
     * @param bool $domain
     * @return string
     */
    function __url(string $url = '', array $vars = [], $suffix = true, $domain = false)
    {
        return url($url, $vars, $suffix, $domain)->build();
    }
}

if (!function_exists('password')) {

    /**
     * 密码加密算法
     * @param $value 需要加密的值
     * @param $type  加密类型，默认为md5 （md5, hash）
     * @return mixed
     */
    function password($value, $salt = '_encrypt')
    {
        $value = sha1('blog_') . md5($value) . md5($salt) . sha1($value);
        return sha1($value);
    }
}

if (!function_exists('xdebug')) {

    /**
     * debug调试
     * @deprecated 不建议使用，建议直接使用框架自带的log组件
     * @param string|array $data 打印信息
     * @param string $type 类型
     * @param string $suffix 文件后缀名
     * @param bool $force
     * @param null $file
     */
    function xdebug($data, $type = 'xdebug', $suffix = null, $force = false, $file = null)
    {
        !is_dir(runtime_path() . 'xdebug/') && mkdir(runtime_path() . 'xdebug/');
        if (is_null($file)) {
            $file = is_null($suffix) ? runtime_path() . 'xdebug/' . date('Ymd') . '.txt' : runtime_path() . 'xdebug/' . date('Ymd') . "_{$suffix}" . '.txt';
        }
        file_put_contents($file, "[" . date('Y-m-d H:i:s') . "] " . "========================= {$type} ===========================" . PHP_EOL, FILE_APPEND);

        $str = '';

        if (is_string($data)) {
            $str = $data;
        } else {
            if (is_array($data) || is_object($data)) {
                $str = print_r($data, true);
            } else {
                $str = var_export($data, true);
            }
        }

        $str . PHP_EOL;

        $force ? file_put_contents($file, $str) : file_put_contents($file, $str, FILE_APPEND);
    }
}

if (!function_exists('sysconfig')) {

    /**
     * 获取系统配置信息
     * @param $group
     * @param null|bool|string $name
     * @return array|mixed
     */
    function sysconfig($group, $name = null, $default = null)
    {

        if ($name === true) {
            $value = Cache::get('sysconfig_' . $group);

            if (empty($value)) {
                $value = \app\admin\model\SystemConfig::where('name', $group)->value('value');
                Cache::tag('sysconfig')->set('sysconfig_' . $group, $value);
            }
            if (is_null($value)) {
                return $default;
            }
            return $value;
        }

        $where = ['group' => $group];
        $value = empty($name) ? Cache::get("sysconfig_{$group}") : Cache::get("sysconfig_{$group}_{$name}");
        if (empty($value)) {
            if (!empty($name)) {
                $where['name'] = $name;
                $value = \app\admin\model\SystemConfig::where($where)->value('value');
                Cache::tag('sysconfig')->set("sysconfig_{$group}_{$name}", $value, 3600);
            } else {
                $value = \app\admin\model\SystemConfig::where($where)->column('value', 'name');
                Cache::tag('sysconfig')->set("sysconfig_{$group}", $value, 3600);
            }
        }
        if (is_null($value)) {
            return $default;
        }
        return $value;
    }
}

if (!function_exists('array_format_key')) {

    /**
     * 二位数组重新组合数据
     * @param $array
     * @param $key
     * @return array
     */
    function array_format_key($array, $key)
    {
        $newArray = [];
        foreach ($array as $vo) {
            $newArray[$vo[$key]] = $vo;
        }
        return $newArray;
    }
}

if (!function_exists('auth')) {

    /**
     * auth权限验证
     * @param $node
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    function auth($node = null)
    {
        $authService = new AuthService(session('admin.id'));
        $check = $authService->checkNode($node);
        return $check;
    }
}


function json_message($data = [], $code = 0, $msg = '')
{
    if (is_string($data)) {

        if (strpos($data, 'http') === 0 || strpos($data, '/') === 0) {
            $data = [
                'jump_to_url' => $data
            ];
        } else {

            $code = $code === 0 ? 500 : $code;
            $msg = $data;
            $data = [];
        }
    } else if ($data instanceof Url) {
        $data = [
            'jump_to_url' => (string)$data
        ];
    }

    return json([
        'code' => $code,
        'msg' => $msg,
        'data' => $data
    ]);
}


function convert_size_readable($size)
{
    if (empty($size)) {
        return $size;
    }
    $unit = array('b', 'KB', 'MB', 'GB', 'TB', 'PB');
    return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
}

function get_dir_size($dir_name)
{
    $dir_size = 0;
    if (is_dir($dir_name)) {
        if ($dh = opendir($dir_name)) {
            while (($file = readdir($dh)) !== false) {
                if ($file != "." && $file != "..") {
                    if (is_file($dir_name . "/" . $file)) {
                        $dir_size += filesize($dir_name . "/" . $file);
                    }
                    /* check for any new directory inside this directory */
                    if (is_dir($dir_name . "/" . $file)) {
                        $dir_size +=  get_dir_size($dir_name . "/" . $file);
                    }
                }
            }
            closedir($dh);
        }
    }
    return $dir_size;
}


function str_to_bin($str)
{
    //1.列出每个字符
    // 这边的分割正则也不理解
    // (?<!^) 后瞻消极断言
    // (?!$) 前瞻消极断言
    // 看意思好像说的是：不以^开头（但是这边 ^ 又没有被转义...），不以 $ 结尾（同上）
    // 然后得到的记过就是字符串一个个被分割成了数组（郁闷）
    // 求解释
    $arr = preg_split('/(?<!^)(?!$)/u', $str);
    //2.unpack字符  
    foreach ($arr as &$v) {
        /**
         * unpack：将二进制字符串解包(英语原文：Unpack data from binary string)
         * H: 英语描述原文：Hex string, high nibble first 
         * 这段代码做了什么？？
         */
        $temp = unpack('H*', $v); // 这边被解析出来的字符串为什么是 16进制的？？
        $v = base_convert($temp[1], 16, 2);
        unset($temp);
    }

    return join('', $arr);
}


function get_host_key()
{
    $host_key_file = App::getRootPath() . '/config/storage/host_key';

    PathTools::intiDir($host_key_file);

    $key = '';
    if (file_exists($host_key_file)) {
        $key = file_get_contents($host_key_file);
    }

    if (empty($key)) {
        $key = uniqid();
        file_put_contents($host_key_file, $key);
    }

    return $key;
}
